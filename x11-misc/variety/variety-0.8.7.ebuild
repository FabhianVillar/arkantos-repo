# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{8..10} )

inherit distutils-r1

DESCRIPTION=" Wallpaper downloader and manager for Linux systems "
HOMEPAGE="https://github.com/varietywalls/variety/"
SRC_URI="https://github.com/varietywalls/variety/archive/refs/tags/0.8.7.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="imagemagick feh nitrogen"
MERGE_TYPE="source"

DEPEND=""
RDEPEND="	x11-libs/gtk+
		 	media-libs/gexiv2
			x11-libs/libnotify
			dev-python/beautifulsoup4
			dev-python/lxml
			dev-python/pycairo
			dev-python/pygobject
			dev-python/configobj
			dev-python/pillow
			dev-python/setuptools
			dev-python/requests
			dev-python/httplib2
			imagemagick? ( media-gfx/imagemagick )
			feh? ( media-gfx/feh )
			nitrogen? ( x11-misc/nitrogen ) "
BDEPEND=" dev-python/python-distutils-extra "

src_unpack() {
	unpack 0.8.7.tar.gz
}

python_compile() {
   esetup.py install --root="${D}"/usr
}
